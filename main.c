/*
 * File:   main.c
 * Author: Wyatt
 *
 * Created on December 26, 2017, 2:18 PM
 */


#include <xc.h>
#include <p24FJ128GB410.h>
#include "system.h"

//Only define 1 Level at a time
//#define Level1      //Digital Inputs to Digtial Outputs
//#define Level2      //Analog inputs 1 to Digital Outputs
#define Level3      //Digtial to Digital Analog to Analog
#define Level4      ///Servo Mode Madness!!!!  


#define DigIn       PORTD
#define DigOut      LATB

//Define each of the analog channels as as number
#define AnaInCh1    16
#define AnaInCh2    21
#define AnaInCh3    22
#define AnaInCh4    23

#define AnaInBuf1   ADC1BUF16
#define AnaInBuf2   ADC1BUF21
#define AnaInBuf3   ADC1BUF22
#define AnaInBuf4   ADC1BUF23

//Analog outputs
#define PWM1        CCP1RB
#define PWM2        CCP4RB
#define PWM3        CCP2RB
#define AnaOut4     DAC1DAT

//Servo Driver registers
#define SVOD1       CCP3RB
#define SVOD2       CCP5RB
#define SVOD3       CCP6RB


//Global Varibles
#if defined(Level4)
unsigned char SERTRACK;
unsigned int SERVOBUFF[16];     //Declare the servo buffers
#endif

//For ease of reading, the setup will be placed inside this routine
void setup (void) {
    
    //IO Setup                  ////////////////////////////////////////////////
    TRISB = 0x0000;             //Set PORTB as all digital outputs
    ANSB = 0x0000;          
    
    TRISD = 0xFFFF;             //Set PORTD as all digital inputs
    ANSD = 0x0000;
    
    TRISG = TRISG & 0x4FFF;
    ANSG = ANSG & 0x4FFF;
    
#if defined(Level2) || defined(Level3)
    //Setup analog inputs       ////////////////////////////////////////////////
    TRISCbits.TRISC4 = 1;       //Analog Input 1
    ANSCbits.ANSC4 = 1;
    TRISEbits.TRISE9 = 1;       //Analog Input 2
    ANSEbits.ANSE9 = 1;
    TRISAbits.TRISA7 = 1;       //Analog Input 3
    ANSAbits.ANSA7 = 1;
    TRISAbits.TRISA6 = 1;       //Analog Input 4
    ANSAbits.ANSA6 = 1;
#endif
    
    //Re-mappable Pins          ////////////////////////////////////////////////
#if defined (Level3)
    RPOR10bits.RP21R = 16;      //Map OCM4 (PWM4) to RP21
#endif
#if defined (Level4)
    RPOR0bits.RP0R = 17;        //Map OCM5 (PWM5) to RP0

#endif
    
#if defined(Level2) || defined(Level3)
    //ADC Setup                 ////////////////////////////////////////////////
    //Timer 1 Setup for Auto conversion start
    T1CON = 0x8000;             //Turn Timer 1 on
    PR1 = 0x3E7F;               //Setup to reset 4000 times a second

    AD1CON1 = 0x0000;           //Clear out the register 
    AD1CON1bits.ADSIDL = 1;     //Stop operation when in IDLE mode
    AD1CON1bits.MODE12 = 1;     //12 Bit conversions 
    AD1CON1bits.SSRC = 5;       //Timer 1 as conversion trigger
    
    AD1CON2 = 0x0000;           //Clear out register
    AD1CON2bits.BUFREGEN = 1;   //Conversion result is placed in corresponding buffer
    AD1CON2bits.CSCNA = 1;      //Scan the inputs
    AD1CON2bits.ALTS = 1;       //Alternate between Buffer A and Buffer B
    AD1CON2bits.SMPI = 3;       //Sample 3 inputs per trigger
    
    AD1CON3 = 0x0000;           //Clear the register
    AD1CON3bits.SAMC = 15;      //Auto-Sample Time Select bits
    AD1CON3bits.ADCS = 16;      //A/D Conversion Clock Select (Divider) bits
    
    AD1CSSL = 0x0000;           //Setup to sample inputs 16, 21, 22, and 23
    AD1CSSH = 0x00E1;           //

    AD1CON1bits.ADON = 1;       //Turn on the ADC Module  
    AD1CON5bits.ASEN = 1;       //Start auto conversion
#endif
    
#if defined (Level3)
    //PWM1 Setup                    ////////////////////////////////////////////
    
   // Set MCCP operating mode 
    CCP1CON1Lbits.CCSEL = 0;        // Set MCCP operating mode (OC mode)
    CCP1CON1Lbits.MOD = 0b0101;     // Set mode (Buffered Dual-Compare/PWM mode) 
    
    //Configure MCCP Timebase 
    CCP1CON1Lbits.TMR32 = 0;        // Set timebase width (16-bit)
    CCP1CON1Lbits.TMRSYNC = 0;      // Set timebase synchronization (Synchronized)
    CCP1CON1Lbits.CLKSEL = 0b000;   // Set the clock source (Tcy)
    CCP1CON1Lbits.TMRPS = 0b00;     // Set the clock pre-scaler (1:1)
    CCP1CON1Hbits.TRIGEN = 0;       // Set Sync/Triggered mode (Synchronous)
    CCP1CON1Hbits.SYNC = 0b00000;   // Select Sync/Trigger source (Self-sync)


    //Configure MCCP output for PWM signal
    CCP1CON2H = 0x0400;             // Enable desired output signals (OC1C)
    CCP1CON3Hbits.OUTM = 0b000;     // Set advanced output modes (Standard output) 
    
    CCP1CON3Hbits.POLACE = 0;       //Configure output polarity (Active High) 
    CCP1TMRL = 0x0000;              //Initialize timer prior to enable module. 

    CCP1PRL = 0x0FFF;               //Configure timebase period

    CCP1RA = 0x0000;                // Set the rising edge compare value
    CCP1RB = 0x1000;                // Set the falling edge compare value
    
    CCP1CON1Lbits.CCPON = 1;        // Turn on MCCP module
 
    //PWM2 Setup
    CCP2CON1L = CCP1CON1L;
    CCP2CON1H = CCP1CON1H;
    CCP2CON3H = CCP1CON3H;
    
    CCP2TMRL = CCP1TMRL;
    CCP2PRL = CCP1PRL;
    
    CCP2RA = CCP1RA;
    CCP2RB = CCP1RB;
    
    CCP2CON1Lbits.CCPON = 1;
    
    //RP21
    //SCCP4 = 16
    //PWM3 Setup
    CCP4CON1L = CCP1CON1L;
    CCP4CON1H = CCP1CON1H;
    CCP4CON3H = CCP1CON3H;
    
    CCP4TMRL = CCP1TMRL;
    CCP4PRL = CCP1PRL;
    
    CCP4RA = CCP1RA;
    CCP4RB = CCP1RB;
    
    CCP4CON1Lbits.CCPON = 1;
    
    //Analog Out4
    DAC1CON = 0x8082;
    DAC1DAT = 0x0200;
#endif
    
#if defined(Level4)
    //Servo Setup
    SERTRACK = 0;
    
    //PWM3 Setup                    ////////////////////////////////////////////
    
   // Set MCCP operating mode 
    CCP5CON1Lbits.CCSEL = 0;        // Set MCCP operating mode (OC mode)
    CCP5CON1Lbits.MOD = 0b0101;     // Set mode (Buffered Dual-Compare/PWM mode) 
    
    //Configure MCCP Timebase 
    CCP5CON1Lbits.TMR32 = 0;        // Set timebase width (16-bit)
    CCP5CON1Lbits.TMRSYNC = 0;      // Set timebase synchronization (Synchronized)
    CCP5CON1Lbits.CLKSEL = 0b000;   // Set the clock source (Tcy)
    CCP5CON1Lbits.TMRPS = 0b01;     // Set the clock pre-scaler (1:1)
    CCP5CON1Hbits.TRIGEN = 0;       // Set Sync/Triggered mode (Synchronous)
    CCP5CON1Hbits.SYNC = 0b00000;   // Select Sync/Trigger source (Self-sync)
    
    CCP5CON3Hbits.OUTM = 0b000;     // Set advanced output modes (Standard output) 
    CCP5CON3Hbits.POLACE = 0;       //Configure output polarity (Active High) 
    
    CCP5TMRL = 0x0000;              //Initialize timer prior to enable module. 

    CCP5PRL = 11427;               //Configure timebase period

    CCP5RA = 1999;                  // Set the rising edge compare value
    CCP5RB = 0x1000;                // Set the falling edge compare value
    
    CCP5CON1Lbits.CCPON = 1;        // Turn on MCCP module
    
    //PWM 5
    CCP6CON1L = CCP5CON1L;
    CCP6CON1H = CCP5CON1H;
    CCP6CON3H = CCP5CON3H;
    
    CCP6TMRL = CCP5TMRL;
    CCP6PRL = CCP5PRL;
    
    CCP6RA = CCP5RA;
    CCP6RB = CCP5RB;
    
#endif
    
    //Interrupt setup
    AD1CON1bits.SAMP = 0;       //Start sampling
        
    IEC2bits.CCT5IE = 1;    //Enable the Servo interrupts
    
    INTCON2bits.GIE = 1;    //Enable all interrupts
    
    return;
}

/*This PIC24 has an interrupt vector for for each interrupt. To define code at an
 * interrupt vector simply write 
 * 
 * "void __attribute__ ((interrupt,auto_psv)) _<INTERRUPTNAME>Interrupt(void) {
 * 
 * replacing <INTERRUPTNAME> with whatever you want to catch like ADC1 or USB1
 * The normally just used the letters before "IE" (e.g. CCT3IE would be _CCPT3Interrupt)
 * 
 * Example:
 void __attribute__ ((interrupt,auto_psv)) _ADC1Interrupt(void) {
    SERVOBUFF[0] = 2;
}
 *                                                                           */
    
//Small subroutine to read back the data from the realigned analog channel
unsigned int readANA(unsigned char Channel) {
    
    switch (Channel) {
        case 1:
            return ADC1BUF16;
        break;
        case 2:
            return ADC1BUF21;
        break;
        case 3:
            return ADC1BUF22;
        break;
        case 4:
            return ADC1BUF23;
        break;
        default:   // This might also be able to be removed but if you do adjust curly braces
            return 0;
        break;  //This can be removed
    }
}

#if defined(Level4)
void __attribute__ ((interrupt,auto_psv)) _CCT5Interrupt(void) {
    
    //LATBbits.LATB13 = !LATBbits.LATB13;
    
    IFS2bits.CCT5IF = 0;        //Clear the interrupt Flag
    SERTRACK = SERTRACK + 1;
    
    //Load in the value of the next servo, direct the signal to the current 
    switch (SERTRACK) {
        case 1:                         //Servo 1
            RPOR0bits.RP0R = 17;
            SVOD2 = SERVOBUFF[1] + 5999;
            
            RPOR3bits.RP7R = 18;
            SVOD3 = SERVOBUFF[8] + 5999;
        break;
        case 2:                         //Servo 2
            RPOR0bits.RP0R = 0;
            RPOR0bits.RP1R = 17;        //Map OCM5 (PWM5) to RP0
            SVOD2 = SERVOBUFF[2] + 5999;
            
            RPOR3bits.RP7R = 0;
            RPOR4bits.RP8R = 18;        //Map OCM5 (PWM5) to RP0
            SVOD3 = SERVOBUFF[9] + 5999;
        break;
        case 3:                         //Servo 3
            RPOR0bits.RP1R = 0;
            RPOR6bits.RP13R = 17;        //Map OCM5 (PWM5) to RP0
            SVOD2 = SERVOBUFF[4] + 5999;
            
            RPOR4bits.RP8R = 0;
            RPOR4bits.RP9R = 18;        //Map OCM5 (PWM5) to RP0
            SVOD3 = SERVOBUFF[14] + 5999;
        break;
        case 4:
            RPOR6bits.RP13R = 0;
            RPOR14bits.RP28R = 17;        //Map OCM5 (PWM5) to RP0
            SVOD2 = SERVOBUFF[5] + 5999;
            
            RPOR4bits.RP9R = 0;
            RPOR7bits.RP14R = 18;        //Map OCM5 (PWM5) to RP0
            SVOD3 = SERVOBUFF[15] + 5999;
        break;
        case 5:
            RPOR14bits.RP28R = 0;
            RPOR9bits.RP18R = 17;        //Map OCM5 (PWM5) to RP0
            SVOD2 = SERVOBUFF[6] + 5999;
            
            RPOR7bits.RP14R = 0;
            RPOR14bits.RP29R = 18;        //Map OCM5 (PWM5) to RP0
            SVOD3 = 0;
        break;
        case 6:
            RPOR9bits.RP18R = 0;
            RPOR3bits.RP6R = 17;        //Map OCM5 (PWM5) to RP0
            SVOD2 = 0;
            
            RPOR14bits.RP29R = 0;        //Map OCM5 (PWM5) to RP0
        break;
        default:
            RPOR3bits.RP6R = 0;         //Map OCM5 (PWM5) to RP0
            SVOD2 = SERVOBUFF[0] + 5999;
            
            SVOD3 = SERVOBUFF[7] + 5999;
            SERTRACK = 0;
        break;
    }
}
#endif

//Main Return
void main(void) {
    
    //Call the setup program
    setup();
        
    //The main Program goes inside this loop
    while (1) {
        
#if defined(Level1) || defined(Level3)
#if defined(Level4)
        DigOut = (DigIn ^ 0xFFFF) & 0x3808;
#else
        DigOut = DigIn ^ 0xFFFF;
#endif
#endif
        
#if defined(Level2)
        DigOut = readANA(1);
        STALED1 = AD1CON1bits.DONE;
#endif
       
#if defined(Level3)
        PWM1 = readANA(1);
        PWM2 = readANA(2);
        PWM3 = readANA(3);
        AnaOut4 = readANA(4) >> 2;
#endif
#if defined (Level4)
        
        unsigned char count;
        for (count = 0; count < 8; count = count + 1) {
            SERVOBUFF[count] = readANA(1);
            SERVOBUFF[count + 8] = readANA(2);
                    
        }
#endif
    }
}



